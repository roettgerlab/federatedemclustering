import json
import os

from engine.app import App, AppState, app_state
from engine.app import Role
from time import sleep
import numpy as np
# from mainGMM import GMM
# from app.examples.blank.mainGMM import GMM
from sklearn.mixture import GaussianMixture
from scipy.stats import multivariate_normal as mvn

# This is the app instance, which holds various values and is used by the app states below
# You shouldn't access this app instance directly, just ignore it for now
app = App()

STATE_INITIAL = 'initial'
STATE_READ_DATA = 'read_data'
STATE_INIT_PARAMETER = "init_param"
STATE_E_STEP = "e_step"
STATE_M_STEP = "m_step"
STATE_TERM = "state_terminate"

#
N_SAMPLES = "n_samples"
N_FEATURES = "n_features"
DATA = "data"
PI = "pi"
MU = "mu"
COV = "cov"
GLOBALK = "global_K"
GLOBALN = "global_N"
GAMMA = "local_gamma"
GAMMA_SUM = "gamma_sum"
ITER = "iter"


@app_state(app, 'initial')
class InitialState(AppState):
    def register(self):
        self.register_transition(STATE_READ_DATA, Role.BOTH)

    def run(self) -> str or None:
        self.app.log("Initial State as " + str(self.app.coordinator))
        # self.app.log(os.listdir('/mnt/input'))
        return STATE_READ_DATA


# here the coordinator initializes mean, variance and weight
@app_state(app, STATE_INIT_PARAMETER, Role.COORDINATOR)
class InitializeParameter(AppState):
    def register(self):
        self.register_transition(STATE_E_STEP, Role.COORDINATOR)

    def run(self) -> str or None:
        self.app.log("InitializeParameter State as " + str(self.app.coordinator))
        self.app.log(str(self.app.data_incoming))
        self.app.log(str(self.app.data_outgoing))
        self.app.log("before client data")
        client_data = self.gather_data()
        self.app.log("after client data")
        self.app.log(str(client_data))
        # self.app.log(str(self.app.internal))

        # based on the locally available data

        self.update(progress=0.)

        # the number of clusters TODO this is hardcoded
        global_k = 3

        # number of objects/samples
        global_n = 0

        self.app.log("trying to build the sum")

        for myClient in client_data:
            global_n = global_n + myClient[N_SAMPLES]

        self.app.log("Global Number of samples: " + str(global_n))

        # number of dimensions/features
        # TODO check that all have the same shape
        d = self.app.internal[DATA].shape[1]

        # initializing mean covariance and weights
        gm = GaussianMixture(n_components=global_k).fit(self.app.internal[DATA])

        # 1 mu => means
        self.app.internal[MU] = gm.means_

        # 2 cov => covariance matrix
        self.app.internal[COV] = gm.covariances_

        # 3 pi => weights
        self.app.internal[PI] = gm.weights_

        # 4 K
        self.app.internal[GLOBALK] = global_k

        # 5 N
        self.app.internal[GLOBALN] = global_n

        self.app.log("Broadcasting")
        self.broadcast_data({MU: gm.means_.tolist(), COV: gm.covariances_.tolist(), PI: gm.weights_.tolist(), GLOBALK: global_k, ITER: 0})
        self.app.log("after broadcasting")

        return STATE_E_STEP


# Input format
# rows = samples
# columns = features

@app_state(app, STATE_E_STEP, Role.BOTH)
class EStep(AppState):
    def register(self):
        self.register_transition(STATE_M_STEP, Role.BOTH)
        self.register_transition(STATE_TERM, Role.BOTH)

    def run(self) -> str or None:
        self.app.log("E-Step as " + str(self.app.coordinator))
        self.app.log(self.app.data_incoming)
        global_parameters = self.await_data()
        self.app.log(str(global_parameters))

        #TODO hardcoded terminator
        if(global_parameters[ITER] > 25):
            return STATE_TERM

        self.app.log(global_parameters[ITER]/25)
        self.update(progress=global_parameters[ITER]/25)


        global_mu = np.array(global_parameters[MU])
        global_cov = np.array(global_parameters[COV])
        global_pi = np.array(global_parameters[PI])
        global_k = global_parameters[GLOBALK]


        # remember global variables
        self.app.internal[ITER] = global_parameters[ITER]
        self.app.internal[COV] = global_cov
        self.app.internal[PI] = global_pi
        self.app.internal[MU] = global_mu

        self.app.log(str(global_mu))
        self.app.log(str(global_cov))
        self.app.log(str(global_pi))
        self.app.log("ITERATOR: "+ str(self.app.internal[ITER]))

        # compute local gamma
        gamma = np.zeros((self.app.internal[N_SAMPLES], global_k))

        # local data
        local_data = self.app.internal[DATA]

        for count in range(global_k):
            gamma[:, count] = global_pi[count] * mvn.pdf(local_data, global_mu[count, :], global_cov[count])

        gamma_norm = np.sum(gamma, axis=1)[:, np.newaxis]
        gamma /= gamma_norm

        self.app.internal[GAMMA] = gamma
        self.app.internal[GLOBALK] = global_k
        self.app.log("Computed gamma")
        self.app.log(str(gamma))

        return STATE_M_STEP


@app_state(app, STATE_M_STEP, Role.BOTH)
class MStep(AppState):

    def register(self):
        self.register_transition(STATE_E_STEP, Role.BOTH)
        self.register_transition(STATE_TERM, Role.BOTH)

    def run(self) -> str or None:
        self.app.log("M-Step as " + str(self.app.coordinator))


        #gathered_data = self.gather_data()
        #gamma_sum = np.zeros()
        #for myClient in gathered_data:
        #    gamma_sum += myClient[GAMMA_SUM]
        #self.app.log("Gamma Sum: \n" + str(gamma_sum))
        self.app.log(str(self.app.internal))
        gamma = self.app.internal[GAMMA]
        data = self.app.internal[DATA]
        global_k = self.app.internal[GLOBALK]
        n_features = self.app.internal[N_FEATURES]

        # local pi update
        local_pi = np.sum(gamma, axis=0)

        # local mu update
        local_mu = np.dot(gamma.T, data)

        # gamma sum
        gamma_sum = np.sum(gamma, axis=0)

        self.app.log("Sending to coordinator")
        self.send_data_to_coordinator({MU: local_mu.tolist(), PI: local_pi.tolist(), GAMMA_SUM: gamma_sum.tolist()})

        self.app.log("Before coordinator computations")
        if self.app.coordinator:
            gathered_data = self.gather_data()
            self.app.log("gathered Data:" + str(gathered_data))
            pi_new = np.zeros(local_pi.shape)
            #self.app.log("PI_NEW: \n" + str(pi_new))
            mu_new = np.zeros(local_mu.shape)
            #self.app.log("MU_NEW: \n" + str(mu_new))
            ga_sum = np.zeros(global_k)
            #self.app.log("GA_SUM: \n" + str(ga_sum))
            for myClient in gathered_data:
                pi_new += np.array(myClient[PI])
                mu_new += np.array(myClient[MU])
                ga_sum += np.array(myClient[GAMMA_SUM])
            mu_new /= ga_sum[:,np.newaxis]
            pi_new /= self.app.internal[GLOBALN]
            self.app.log("PI_NEW: \n" + str(pi_new))
            self.app.log("MU_NEW: \n" + str(mu_new))
            self.app.log("GA_SUM: \n" + str(ga_sum))
            self.app.internal[MU] = mu_new
            self.app.internal[PI] = pi_new
            self.broadcast_data({MU: mu_new.tolist(), PI: pi_new.tolist()})

        received = self.await_data()
        mu_new = np.array(received[MU])
        pi_new = np.array(received[PI])

        # local covariance
        cov_new = np.zeros((global_k, n_features, n_features))
        for count in range(global_k):
            x_mu = np.matrix(data - mu_new[count,:])
            gamma_diag = np.matrix(np.diag(gamma[:,count]))
            sigma_c = x_mu.T * gamma_diag * x_mu
            cov_new[count, :, :] = sigma_c
        self.send_data_to_coordinator({COV: cov_new.tolist()})

        if self.app.coordinator:
            gathered_data = self.gather_data()
            cov_new = np.zeros((global_k, n_features, n_features))

            for count in range(global_k):
                for myClient in gathered_data:
                    client_cov = np.array(myClient[COV])
                    cov_new[count, :, :] += client_cov[count]
                cov_new[count, :, :] /= np.sum(gamma, axis=0)[:, np.newaxis][count]

            self.app.log("OLD Covariance: \n" + str(self.app.internal[COV]))
            self.app.log("NEW Covariance: \n" + str(cov_new))

            self.broadcast_data({MU: mu_new.tolist(), COV: cov_new.tolist(), PI: pi_new.tolist(), GLOBALK: global_k, ITER: (self.app.internal[ITER] + 1)})

        return STATE_E_STEP



@app_state(app, STATE_TERM, Role.BOTH)
class Terminate(AppState):

    def run(self) -> str or None:
        self.app.log("Terminate as " + str(self.app.coordinator))
        output_folder = '/mnt/output/'

        cov = self.app.internal[COV]
        for count in range(cov.shape[0]):
            np.savetxt(output_folder + "cov_" + str(count) + ".txt", cov[count,:,:], delimiter="\t")
        np.savetxt(output_folder + "pi.txt", self.app.internal[PI], delimiter="\t")
        np.savetxt(output_folder + "mu.txt", self.app.internal[MU], delimiter="\t")
        return None


# everyone reads that data that should be provided locally here
@app_state(app, STATE_READ_DATA, Role.BOTH)
class ReadData(AppState):
    def register(self):
        self.register_transition(STATE_INIT_PARAMETER, Role.COORDINATOR)
        self.register_transition(STATE_E_STEP, Role.PARTICIPANT)

    def run(self) -> str or None:
        self.app.log("ReadData State as " + str(self.app.coordinator))
        path = '/mnt/input'
        data_path = '/mnt/input/data.csv'
        os.listdir(path)
        input_data = np.genfromtxt(data_path)
        sample_count = input_data.shape[0]
        feature_count = input_data.shape[1]

        self.app.internal[DATA] = input_data
        self.app.internal[N_SAMPLES] = sample_count
        self.app.internal[N_FEATURES] = feature_count
        self.app.log("SENDING DATA TO THE COORDINATOR")
        self.send_data_to_coordinator({N_SAMPLES: sample_count, N_FEATURES: feature_count})
        self.app.log("DONE SENDING DATA TO THE COORDINATOR")

        if self.app.coordinator:
            return STATE_INIT_PARAMETER
        else:
            self.app.log("Before E STep")
            return STATE_E_STEP
